function getMatchesWonPerTeamPerYear(data) {
    var collectivedata = {};
    for(var i=0;i<data.length;i++){
        if(!(data[i].season in collectivedata)){
            var yeardata = {};
            for(var j =0;j<data.length;j++){
                if(!data[j].winner)
                    continue;
                if(data[i].season == data[j].season){
                    if(data[j].winner in yeardata){
                        yeardata[data[j].winner]++;
                    }
                    else{
                        yeardata[data[j].winner] = 1;
                    }
                }
            }
            collectivedata[data[i].season] = yeardata;
        }
    }

    return collectivedata;

}
module.exports = {
    getMatchesWonPerTeamPerYear: getMatchesWonPerTeamPerYear
}