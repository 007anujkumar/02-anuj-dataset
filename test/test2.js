var expect = require("chai").expect;
var dataSet = require('../src/api/dataSet2.js')



describe('value', function () {
    it('It should return the matches won', function () {
        var input = [
            {
                id: '1',
                season: '2008',
                team1: 'Sunrisers Hyderabad',
                team2: 'Royal Challengers Bangalore',
                winner: 'Sunrisers Hyderabad'
            },
            {
                id: '2',
                season: '2008',
                team1: 'Delhi Daredevils',
                team2: 'Royal Challengers Bangalore',
                winner: 'Royal Challengers Bangalore'
            },
            {
                id: '3',
                season: '2008',
                team1: 'Mumbai Indians',
                team2: 'Kings XI Punjab',
                winner: 'Kings XI Punjab'
            },
            {
                id: '4',
                season: '2009',
                team1: 'Delhi Daredevils',
                team2: 'Royal Challengers Bangalore',
                winner: 'Delhi Daredevils'
            },
            {
                id: '5',
                season: '2009',
                team1: 'Chennai Super Kings',
                team2: 'Royal Challengers Bangalore',
                winner: 'Royal Challengers Bangalore'
            },
            {
                id: '6',
                season: '2009',
                team1: 'Chennai Super Kings',
                team2: 'Mumbai Indians',
                winner: 'Chennai Super Kings'
            },
            {
                id: '7',
                season: '2010',
                team1: 'Kings XI Punjab',
                team2: 'Rajasthan Royals',
                winner: 'Rajasthan Royals'
            }
        ];

        var results = {
            '2008': {
                'Sunrisers Hyderabad': 1,
                'Royal Challengers Bangalore': 1,
                'Kings XI Punjab': 1
            },
            '2009': {
                'Delhi Daredevils': 1,
                'Royal Challengers Bangalore': 1,
                'Chennai Super Kings': 1
            },
            '2010': { 'Rajasthan Royals': 1 }
        };
        expect(results).deep.equals(dataSet.getMatchesWonPerTeamPerYear(input));




    })
})
